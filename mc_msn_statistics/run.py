from __future__ import annotations

import os
import re
from collections import defaultdict
from datetime import datetime, timedelta
from enum import Enum
import matplotlib.pyplot as plt
from dateutil.rrule import rrule, HOURLY

SOURCES_FOLDER = "../sources/"
CAP_FILES = -1


class EventType(Enum):
    LOGIN = 1
    LOGOUT = 2


class AnalysisPoint:
    def __init__(self, time: datetime, duration: timedelta):
        self.time = time
        self.duration = duration

    def __str__(self):
        return f"{self.time}|{self.duration_print_hours()}h"

    def duration_print_hours(self):
        return round(self.duration.total_seconds() / 3600, 2)


class Event:
    def __init__(self, event_type: EventType, user: str, time: str | datetime):
        self.event_type = event_type
        self.user = user
        self.time = time

    def __str__(self):
        return f"{self.event_type.name}|{self.user}|{self.time}"


def retrieve_event(line):
    if ('joined the game' in line) and ("<" not in line):
        x = line.split(" joined")
        y = x[0].split("[Server thread/INFO]: ")
        username = y[1]
        return Event(EventType.LOGIN, username, y[0][1:-2])
    elif ('left the game' in line) and ("<" not in line):
        x = line.split(" left")
        y = x[0].split("[Server thread/INFO]: ")
        username = y[1]
        return Event(EventType.LOGOUT, username, y[0][1:-2])
    return


def fill_in_events_list(events, lines, date):
    for line in lines:
        e = retrieve_event(line)
        if e is not None and e.user != "AxidoysNO":  # AxidoysNO for blacklist
            events.append(Event(e.event_type, e.user, datetime.strptime(f"{date} {e.time}", "%Y-%m-%d %H:%M:%S")))


def retrieve_date_from_log_file(file_path: str):
    file_name = os.path.basename(file_path)
    date_string = re.search(r"\d{4}-\d{2}-\d{2}", file_name).group()

    return date_string


def groupby_not_sorted(events):
    players_events = defaultdict(list)
    for e in events:
        players_events[e.user].append(e)
    return players_events.items()


def analyse_events(events: list) -> dict[str, list[AnalysisPoint]]:
    players_stats = defaultdict(list)

    for user, user_events in groupby_not_sorted(events):
        last_e = user_events.pop(0)
        for e in user_events:
            if e.event_type == last_e.event_type:
                raise Exception("Should not be 2 times in a row same event type")
            elif e.event_type == EventType.LOGIN:
                pass
            elif e.event_type == EventType.LOGOUT:
                if e.time < last_e.time:
                    raise Exception("Events are not ordered")
                players_stats[user].append(AnalysisPoint(e.time, e.time - last_e.time))
            last_e = e

    return players_stats


def extract_events(log_files: list):
    # events ordered, as the log_files !
    events = []
    for file in log_files:
        log_file = open(file, "r")
        lines = [line.rstrip('\n') for line in log_file.readlines() if line.strip() != '']

        date = retrieve_date_from_log_file(file)

        fill_in_events_list(events, lines, date)
    return events


def plot_cumulatives(cumulatives: dict[str, list[AnalysisPoint]], ylabel=""):
    fig, ax = plt.subplots()
    ax.set_xlabel('Timeline')
    ax.set_ylabel(ylabel)

    for username, cumulative_time in cumulatives.items():
        x = [event.time for event in cumulative_time]
        y = [event.duration.total_seconds() / 3600 for event in cumulative_time]
        ax.plot(x, y, label=username)

    ax.legend()
    plt.show()

    return


def compute_cumulative(analysis: dict[str, list[AnalysisPoint]]) -> dict[str, list[AnalysisPoint]]:
    cumulatives = defaultdict(list)
    for user, a_points in analysis.items():
        last_duration = timedelta()
        for analysis_point in a_points:
            new_cumul = analysis_point.duration + last_duration
            cumulatives[user].append(AnalysisPoint(analysis_point.time, new_cumul))
            last_duration = new_cumul
    return cumulatives


def moving_average(datetime, data, window_size):
    time_spent_sum = timedelta()
    window_size_dt = timedelta(hours=window_size)
    for m_datetime in rrule(HOURLY, dtstart=datetime-window_size_dt, until=datetime+window_size_dt):
        time_spent_sum += data.get(m_datetime, timedelta())

    return time_spent_sum

def compute_moving_average(analysis: dict[str, list[AnalysisPoint]], window_size) -> dict[str, list[AnalysisPoint]]:
    time_spent = defaultdict(list)

    start_date = min([a[0].time for a in analysis.values()]).date()
    end_date = max([a[-1].time for a in analysis.values()]).date()

    for user, a_points in analysis.items():
        hours_points = defaultdict(timedelta)
        for ap in a_points:
            dt = ap.time
            datetime_rounded = dt + timedelta(minutes=-dt.minute, seconds=-dt.second, microseconds=-dt.microsecond)
            hours_points[datetime_rounded] = ap.duration

        for current_datetime in rrule(HOURLY, dtstart=start_date, until=end_date):
            time_spent[user].append(
                AnalysisPoint(
                    current_datetime,
                    moving_average(current_datetime, hours_points, window_size)
                )
            )

    return time_spent


if __name__ == "__main__":
    print("Start")

    log_files = os.listdir(SOURCES_FOLDER)
    log_files = [f for f in log_files if f.endswith(".log")]
    log_files.sort()
    if CAP_FILES > 0:
        log_files = log_files[0:CAP_FILES]
    log_files = [SOURCES_FOLDER + f for f in log_files]

    events = extract_events(log_files)

    analysis = analyse_events(events)

    cumulatives = compute_cumulative(analysis)
    plot_cumulatives(cumulatives, ylabel="Cumulative Time Spent on Server (in hours)")

    surrounding_hours = 168
    sliding_average = compute_moving_average(analysis, surrounding_hours)
    plot_cumulatives(sliding_average, ylabel="Time Spent on Server (in hours/week)")

    print("Finished")