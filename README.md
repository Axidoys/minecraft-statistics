# Minecraft Statistics

Purpose : want to play a bit with the data I had under hands with my Minecraft server.

Using the logs of the server as input.

## How to retrieve the logs :

```shell
scp user@server:/data/minecraft-data/logs/* ./sources/
rm ./sources/latest.log
gzip -d ./sources/*.gz
```

Result of cumulative :

![result of cumulative](result/cumulative_result.png)

Result of sliding average :

![result of sliding average](result/sliding_average.png)


## Personal feedback

Wanted to try again Poetry, but there I didn't benefit of it, as it is more for packaging than anything else.

I tried ChatGPT, had some useful tips, for using datetime or syntax basics. Tried to have more, but got some misunderstandings in the algorithms. Maybe for those big request I could put more context and really explicit conditions, data structures, etc. .

For the Python aspect, discovered some handy features, but still missing the functional style I have in Kotlin.

Also the "sliding average" is maybe not well implemented : not beautiful, not well optimized, not that readable. I would have liked to find a lib to do that fully (maybe pyplot, a smooth option).

I have been suggested to retrieve more kind of data as distance travelled, block placed, destroyed, number of deaths, etc. . Could be nice indeed ! But can't retrieve them from the logs.
